export class WeddingInfo {
    public email: string;
    public weddingName: string;
    public groomFname: string;
    public groomMname: string;
    public groomLname: string;
    public groomDateOfBirth: number;
    public groomPhoneNum: number;
    public groomGender: string;
    public brideFname: string;
    public brideMname: string;
    public brideLname: string;
    public brideDateOfBirth: number;
    public bridePhoneNum: number;
    public brideGender: string;
}
export class EventDesc {
    public ceremonyType: string;
    public hotel: string;
    public checkInDate: number;
    public checkOutDate: number;
    public honeymoon: boolean;
    public bookedRoom: boolean;
    public roomNumber: number;
}
export class GuestList {
    public id: number;
    public name: string;
    public middleName: string;
    public lastName: string;
    public phone: number;
    public mail: string;
    public side: string;
}
