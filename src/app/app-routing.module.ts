import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AccountComponent } from './pages/account/account.component';
import { CalendarComponent } from './pages/calendar/calendar.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { EventsComponent } from './pages/events/events.component';
import { HotelsComponent } from './pages/hotels/hotels.component';
import { MessagesComponent } from './pages/messages/messages.component';
import { NotesComponent } from './pages/notes/notes.component';

const routes: Routes = [
  {path: '', component: DashboardComponent},
  {path: 'calendar', component: CalendarComponent},
  {path: 'hotels', component: HotelsComponent},
  {path: 'my-events', component: EventsComponent},
  {path: 'notes', component: NotesComponent},
  {path: 'messages', component: MessagesComponent},
  {path: 'my-account', component: AccountComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
