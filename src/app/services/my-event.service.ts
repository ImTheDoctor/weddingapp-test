import { Injectable } from '@angular/core';
import { WeddingInfo, EventDesc, GuestList } from '../info';

@Injectable({providedIn: 'root'})
export class MyEventService {

  public weddingInfo(info: WeddingInfo): void{
    console.log(info);
  }

  public eventDesc(desc: EventDesc): void{
    console.log(desc);
  }

  public guestList(guest: GuestList): void{
    console.log(guest);
  }
}

