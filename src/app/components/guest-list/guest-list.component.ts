import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { GuestList } from 'src/app/info';
import { MyEventService } from 'src/app/services/my-event.service';

@Component({
  selector: 'app-guest-list',
  templateUrl: './guest-list.component.html',
  styleUrls: ['./guest-list.component.scss'],
  providers: [MyEventService]
})
export class GuestListComponent implements OnInit {
  @Output() nextPage = new EventEmitter();
  @Output() prevPage = new EventEmitter();

  constructor(public myEventService: MyEventService) {}
  model = new GuestList();
  data = [];

  public addValue(): void {
    this.myEventService.guestList(this.model);
    this.data.push(this.model);
    this.model = new GuestList();
  }

  public next(): void{
    this.nextPage.emit();
  }

  public prev(): void{
    this.prevPage.emit();
  }

  ngOnInit(): void {
  }

}
