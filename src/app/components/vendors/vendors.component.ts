import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { MyEventService } from 'src/app/services/my-event.service';

@Component({
  selector: 'app-vendors',
  templateUrl: './vendors.component.html',
  styleUrls: ['./vendors.component.scss']
})
export class VendorsComponent implements OnInit {
  @Output() prevPage = new EventEmitter();

  constructor(private myEventService: MyEventService) {}
  showIt = false;

  public prev(): void{
    this.prevPage.emit();
  }
  public showModal(): void{
    this.showIt = !this.showIt;
  }
  public closeModal(): void{
    this.showIt = false;
  }
  ngOnInit(): void {
  }

}
