import { Component, Output, EventEmitter } from '@angular/core';
import { MyEventService } from 'src/app/services/my-event.service';
import { WeddingInfo } from '../../info';

@Component({
  selector: 'app-merried-info',
  templateUrl: './merried-info.component.html',
  styleUrls: ['./merried-info.component.scss'],
  providers: [MyEventService]
})
export class MerriedInfoComponent {
  @Output() nextPage = new EventEmitter();
  model = new WeddingInfo();
  constructor(public myEventService: MyEventService) {}

  public next(): void{
    this.nextPage.emit();
    this.myEventService.weddingInfo(this.model);
  }
}

