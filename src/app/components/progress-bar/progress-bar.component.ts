import { Component, EventEmitter, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-progress-bar',
  templateUrl: './progress-bar.component.html',
  styleUrls: ['./progress-bar.component.scss']
})
export class ProgressBarComponent implements OnInit {
  @Input() current: EventEmitter<any> = new EventEmitter();

  ngOnInit(): void {
  }
}
