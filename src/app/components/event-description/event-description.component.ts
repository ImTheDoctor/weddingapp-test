import { Component, EventEmitter, Output } from '@angular/core';
import { MyEventService } from 'src/app/services/my-event.service';
import { EventDesc } from '../../info';

@Component({
  selector: 'app-event-description',
  templateUrl: './event-description.component.html',
  styleUrls: ['./event-description.component.scss'],
  providers: [MyEventService]
})
export class EventDescriptionComponent{
  @Output() nextPage = new EventEmitter();
  @Output() prevPage = new EventEmitter();

  constructor(public myEventService: MyEventService) {}
  model = new EventDesc();

  public prev(): void{
    this.prevPage.emit();
  }

  public next(): void{
    this.nextPage.emit();
    this.myEventService.eventDesc(this.model);
  }
}
