import {AfterViewInit, Component, EventEmitter, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-events',
  templateUrl: './events.component.html',
  styleUrls: ['./events.component.scss']
})
export class EventsComponent implements AfterViewInit {
  current = new EventEmitter();
  currentPage = 1;

  public next(): void{
    ++this.currentPage;
    this.current.emit(this.currentPage);
  }

  public prev(): void{
    --this.currentPage;
    this.current.emit(this.currentPage);
  }

  ngAfterViewInit(): void {
      this.current.emit(1);
  }
}
