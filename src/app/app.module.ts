import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { ProgressBarComponent } from './components/progress-bar/progress-bar.component';
import { MerriedInfoComponent } from './components/merried-info/merried-info.component';
import { EventDescriptionComponent } from './components/event-description/event-description.component';
import { GuestListComponent } from './components/guest-list/guest-list.component';
import { VendorsComponent } from './components/vendors/vendors.component';
import { AccountComponent } from './pages/account/account.component';
import { MessagesComponent } from './pages/messages/messages.component';
import { NotesComponent } from './pages/notes/notes.component';
import { EventsComponent } from './pages/events/events.component';
import { HotelsComponent } from './pages/hotels/hotels.component';
import { CalendarComponent } from './pages/calendar/calendar.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { ModalComponent } from './components/modal/modal.component';


@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    CalendarComponent,
    HotelsComponent,
    EventsComponent,
    NotesComponent,
    MessagesComponent,
    AccountComponent,
    NavbarComponent,
    ProgressBarComponent,
    MerriedInfoComponent,
    EventDescriptionComponent,
    GuestListComponent,
    VendorsComponent,
    ModalComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    BrowserAnimationsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
